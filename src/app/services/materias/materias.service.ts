import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MateriasService {

  private API_SERVER = "http://localhost:8090/api/materia";

  constructor( private httpClient: HttpClient ) { }

  public getAllMaterias(): Observable<any>{
    return this.httpClient.get(this.API_SERVER)
  }
}
