import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DatePipe } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class CalificacionesService {

  private API_SERVER = "http://localhost:8090/api/calificacion";

  constructor( private httpClient: HttpClient, private datePipe: DatePipe ) { }

  /*public getAllCalificaciones(usuarioId:any): Observable<any>{
    const params = new HttpParams().set('usuarioId', usuarioId);
    return this.httpClient.get(this.API_SERVER, {params}).pipe(catchError(this.handleError));
  }*/
  public getAllCalificaciones(usuarioId:any): Observable<any>{
    return this.httpClient.get(this.API_SERVER+'/'+usuarioId).pipe(catchError(this.handleError));
  }

  public saveCalificacion (calificacion:any): Observable<any>{
    // Formatear la fecha al formato deseado
    const formattedDate = this.datePipe.transform(new Date(), 'dd/MM/yyyy');

    // Actualizar la propiedad fecha_registro con la fecha formateada
    console.log(calificacion);
    calificacion.fecha_registro = formattedDate; 
    return !calificacion.id_t_calificaciones || (calificacion.id_t_calificaciones+"").trim().length === 0?this.httpClient.post(this.API_SERVER,calificacion).pipe(catchError(this.handleError)):this.httpClient.put(this.API_SERVER+'/'+calificacion.id_t_calificaciones,calificacion).pipe(catchError(this.handleError));
  }

  public deleteCalificacion(calificacionId:any): Observable<any>{
    return this.httpClient.delete(this.API_SERVER+'/'+calificacionId).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log(error.error.message)

    } else {
      console.log(error.status)
    }
    return throwError(
      console.log('Something has happened; Api is not working!'));
  };
}
