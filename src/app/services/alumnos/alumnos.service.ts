import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  private API_SERVER = "http://localhost:8090/api/alumno";

  constructor( private httpClient: HttpClient ) { }

  public getAllAlumnos(): Observable<any>{
    return this.httpClient.get(this.API_SERVER)
  }
}
