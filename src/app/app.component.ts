import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalificacionesService } from './services/calificaciones/calificaciones.service';
import { MateriasService } from './services/materias/materias.service';
import { AlumnosService } from './services/alumnos/alumnos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}) 
export class AppComponent implements OnInit{
  
  calificacionesForm!: FormGroup;
  materia: any;
  alumno: any;
  calificaciones: any;
  promedio:any="-";

  constructor(
    public fb: FormBuilder,
    public calificacionesService: CalificacionesService,
    public materiasService: MateriasService,
    public alumnosService: AlumnosService,
  ){
  }
  ngOnInit(): void {
    this.calificacionesForm=this.fb.group({
      id_t_calificaciones:[''],
      calificacion:['',Validators.required],
      //fecha_registro:['',Validators.required],
      id_t_materias:['',Validators.required],
      id_t_usuarios:['',Validators.required],
    })
    this.materiasService.getAllMaterias().subscribe(resp=>{
      this.materia = resp; 
    },
      error=>(console.error(error))
    )
    this.alumnosService.getAllAlumnos().subscribe(resp=>{
      this.alumno = resp; 
    },
      error=>(console.error(error))
    )

    this.calificacionesForm.get('id_t_usuarios')?.valueChanges.subscribe(value=>{
      if (value != null) {
        this.calificacionesService.getAllCalificaciones(value).subscribe(resp=>{
          this.calificaciones =  resp.slice(0, resp.length - 1);
          this.promedio = resp[resp.length - 1].promedio;
        },
          error=>(console.error(error))
        )
      }else{
        this.calificaciones = [];
        this.promedio = '-';
      }
    })
  }
  guardar(): void {
    var id_t_usuarios_old = this.calificacionesForm.value.id_t_usuarios;
    this.calificacionesService.saveCalificacion(this.calificacionesForm.value).subscribe(resp=>{
      // Restablecer el formulario
      this.calificacionesForm.reset();
      
      // Asignar un valor al campo id_t_usuarios
      this.calificacionesForm.patchValue({
        id_t_usuarios: id_t_usuarios_old
      });
    },
      error=>(console.error(error))
    )
  }
  eliminar(calificacion_materia:any){
    this.calificacionesService.deleteCalificacion(calificacion_materia.id_t_calificaciones).subscribe(resp=>{
      // Asignar un valor al campo id_t_usuarios
      this.calificacionesForm.patchValue({
        id_t_usuarios: this.calificacionesForm.value.id_t_usuarios
      });
    },
      error=>(console.error(error))
    )
  }
  editar(calificacion_materia:any): void {
    this.calificacionesForm.setValue({
      id_t_calificaciones:calificacion_materia.id_t_calificaciones,
      calificacion:calificacion_materia.calificacion,
      //fecha_registro:calificacion.fecha_registro,
      id_t_materias:calificacion_materia.id_t_materias,
      id_t_usuarios:calificacion_materia.id_t_usuarios,
    });
  }
  nuevo(): void {
  }
}
